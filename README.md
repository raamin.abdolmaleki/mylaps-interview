# Mylaps Interview

This project is based on Kotlin and MVI+Clean architecture. 

There are some comments that I think I must mention:  
**In SearchUseCase =>** I found my solution is not optimized and must use reactive tools 
like coroutines channel, flow, or RxJava. But because you said that I spend 8 hours, I ignored it.  
**Tests =>** I Ignored writing tests because I didn't have enough time.  
**Architecture =>** I know simple applications like this don't need complex architecture, 
but I preferred to show what I know because it's an interview test.

