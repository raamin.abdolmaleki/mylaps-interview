package com.ramin.interview.feature.search.domain.repository

import com.ramin.interview.feature.search.data.model.PhotoLocationDataModel
import com.ramin.interview.feature.search.data.model.SearchPhotoDataModel
import com.ramin.interview.library.base.data.Resource

interface SearchRepository {
    suspend fun getPhotos(
        lat: Double,
        lon: Double,
        query: String,
    ): Resource<SearchPhotoDataModel>

    suspend fun getPhotoLocation(
        photoId: String
    ): Resource<PhotoLocationDataModel>
}
