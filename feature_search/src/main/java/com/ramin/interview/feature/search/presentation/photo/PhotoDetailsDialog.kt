package com.ramin.interview.feature.search.presentation.photo

import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.ramin.interview.feature.search.R
import com.ramin.interview.feature.search.data.model.SearchPhoto
import com.ramin.interview.feature.search.data.model.getLargeUrl
import com.ramin.interview.library.base.presentation.fragment.InjectionDialogFragment
import kotlinx.android.synthetic.main.dialog_photo_details.*

class PhotoDetailsDialog private constructor() :
        InjectionDialogFragment(R.layout.dialog_photo_details) {

    lateinit var searchPhoto: SearchPhoto

    companion object {
        fun newInstance(searchPhoto: SearchPhoto): PhotoDetailsDialog {
            val dialog = PhotoDetailsDialog()
            dialog.initialize(searchPhoto)
            return dialog
        }
    }

    fun initialize(
            searchPhoto: SearchPhoto
    ) {
        this.searchPhoto = searchPhoto
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        title.text = searchPhoto.title
        photoId.text = searchPhoto.id
        owner.text = searchPhoto.owner

        ivClose.setOnClickListener {
            dismiss()
        }

        Glide.with(this)
                .load(searchPhoto.getLargeUrl())
                .placeholder(R.drawable.ic_placeholder)
                .into(photo)
    }
}
