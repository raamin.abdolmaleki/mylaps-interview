package com.ramin.interview.feature.search.presentation.map

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnCameraIdleListener
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.pawegio.kandroid.visible
import com.ramin.interview.feature.search.R
import com.ramin.interview.feature.search.data.model.LocationPhoto
import com.ramin.interview.feature.search.data.model.SearchPhoto
import com.ramin.interview.feature.search.data.model.getMarkerUrl
import com.ramin.interview.feature.search.presentation.photo.PhotoDetailsDialog
import com.ramin.interview.library.base.presentation.extension.observe
import com.ramin.interview.library.base.presentation.fragment.InjectionFragment
import com.ramin.interview.library.base.utils.DebounceQueryTextListener
import kotlinx.android.synthetic.main.fragment_map_search.*
import kotlinx.android.synthetic.main.fragment_map_search.view.*
import org.kodein.di.generic.instance

class MapSearchFragment : InjectionFragment(R.layout.fragment_map_search),
    OnMapReadyCallback,
    OnMarkerClickListener,
    OnCameraIdleListener {

    private val viewModel: SearchViewModel by instance()

    private var _googleMap: GoogleMap? = null
    private val googleMap: GoogleMap
        get() = requireNotNull(_googleMap)

    private val stateObserver = Observer<SearchViewModel.ViewState> {

        pgLoading.visible = it.isLoading

        when {
            it.iSuccess -> {
                googleMap.clear()
                it.data!!.search.photos.photo.forEachIndexed { index, searchPhoto ->
                    loadImageAsMarker(searchPhoto, it.data.photo[index].photo)
                }
            }
            it.isError -> {
                // TODO: Handle Errors Here
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mapFragment = childFragmentManager.findFragmentById(R.id.searchMap)
        check(mapFragment is SupportMapFragment)
        mapFragment.getMapAsync(this)

        searchQuery.setOnQueryTextListener(
            DebounceQueryTextListener(this.lifecycle, viewModel::queryChanged)
        )

        searchQuery.setQuery(viewModel.searchQuery, false)

        observe(viewModel.stateLiveData, stateObserver)
        viewModel.loadData()
    }


    private fun loadImageAsMarker(
        searchPhoto: SearchPhoto,
        locationPhoto: LocationPhoto
    ) {
        Glide.with(requireContext())
            .asBitmap()
            .load(searchPhoto.getMarkerUrl())
            .dontTransform()
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    val marker = googleMap.addMarker(
                        MarkerOptions()
                            .icon(BitmapDescriptorFactory.fromBitmap(resource))
                            .position(
                                LatLng(
                                    locationPhoto.location.latitude,
                                    locationPhoto.location.longitude
                                )
                            )
                    )
                    marker.tag = locationPhoto.id
                }

                override fun onLoadCleared(placeholder: Drawable?) {

                }
            })
    }

    override fun onMapReady(map: GoogleMap?) {
        _googleMap = map ?: return

        with(map) {
            setOnCameraIdleListener(this@MapSearchFragment)
            setOnMarkerClickListener(this@MapSearchFragment)
            moveCamera(CameraUpdateFactory.newLatLngZoom(viewModel.latLon, 15f))
        }
    }

    override fun onCameraIdle() {
        googleMap.apply {
            viewModel.mapMoved(
                cameraPosition.target.latitude,
                cameraPosition.target.longitude
            )
        }
    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        marker?.apply {
            viewModel.getPhoto(marker.tag as String)?.apply {
                PhotoDetailsDialog.newInstance(this)
                    .show(parentFragmentManager, "DETAILS")
            }
        }
        return false
    }
}
