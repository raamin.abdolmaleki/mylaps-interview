package com.ramin.interview.feature.search.presentation.map

import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import com.ramin.interview.feature.search.data.model.SearchPhoto
import com.ramin.interview.feature.search.domain.model.SearchPhotoDomainModel
import com.ramin.interview.feature.search.domain.usecase.SearchUseCase
import com.ramin.interview.feature.search.presentation.map.SearchViewModel.Action
import com.ramin.interview.feature.search.presentation.map.SearchViewModel.ViewState
import com.ramin.interview.library.base.data.StatusCode
import com.ramin.interview.library.base.presentation.viewmodel.BaseAction
import com.ramin.interview.library.base.presentation.viewmodel.BaseViewModel
import com.ramin.interview.library.base.presentation.viewmodel.BaseViewState
import kotlinx.coroutines.launch

internal class SearchViewModel(
    private val searchUseCase: SearchUseCase
) : BaseViewModel<ViewState, Action>(ViewState()) {

    // defaults
    var searchQuery = "Motorcycle"
    var latLon = LatLng(52.3546449, 4.8339212)

    var domainModel: SearchPhotoDomainModel? = null

    override fun onLoadData() {
        getSearchResult()
    }

    override fun onReduceState(viewAction: Action) = when (viewAction) {
        is Action.SearchSuccess -> ViewState(
            iSuccess = true,
            data = viewAction.data
        )
        is Action.SearchLoading -> ViewState(isLoading = true)
        is Action.SearchFailure -> ViewState(isError = true)
    }

    private fun getSearchResult() {
        sendAction(Action.SearchLoading)

        viewModelScope.launch {
            searchUseCase.execute(
                query = searchQuery,
                lat = latLon.latitude,
                lon = latLon.longitude
            ).also {
                if (it.statusCode == StatusCode.OK) {
                    domainModel = it.response!!
                    sendAction(Action.SearchSuccess(domainModel!!))
                } else {
                    sendAction(Action.SearchFailure)
                }
            }
        }

    }

    fun getPhoto (id: String): SearchPhoto? {
        domainModel?.apply {
           return search.photos.photo.firstOrNull { it.id == id }
        }
        return null
    }

    fun mapMoved(lat: Double, lon: Double) {
        latLon = LatLng(lat, lon)
        loadData()
    }

    fun queryChanged(newQuery: String?) {
        if (!newQuery.isNullOrEmpty()) {
            searchQuery = newQuery
            loadData()
        }
    }

    internal data class ViewState(
        val isLoading: Boolean = false,
        val isError: Boolean = false,
        val iSuccess: Boolean = false,
        val data: SearchPhotoDomainModel? = null
    ) : BaseViewState

    internal sealed class Action : BaseAction {
        class SearchSuccess(val data: SearchPhotoDomainModel) : Action()
        object SearchFailure : Action()
        object SearchLoading : Action()
    }
}
