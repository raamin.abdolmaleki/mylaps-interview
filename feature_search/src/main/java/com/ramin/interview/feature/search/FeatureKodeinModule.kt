package com.ramin.interview.feature.search

import com.ramin.interview.app.feature.KodeinModuleProvider
import com.ramin.interview.feature.search.data.dataModule
import com.ramin.interview.feature.search.domain.domainModule
import com.ramin.interview.feature.search.presentation.presentationModule
import org.kodein.di.Kodein

internal const val MODULE_NAME = "Search"

object FeatureKodeinModule : KodeinModuleProvider {

    override val kodeinModule = Kodein.Module("${MODULE_NAME}Module") {
        import(presentationModule)
        import(domainModule)
        import(dataModule)
    }
}
