package com.ramin.interview.feature.search.data.retrofit.service

import com.ramin.interview.feature.search.data.model.PhotoLocationDataModel
import com.ramin.interview.feature.search.data.model.SearchPhotoDataModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

internal interface SearchRetrofitService {
    @GET("?method=flickr.photos.search")
    suspend fun getPhotosAsync(
        @Query("text") text: String,
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("radius") radius: Float = 1f,
        @Query("radius_units") radiusUnit: String = "km",
    ): Response<SearchPhotoDataModel>

    @GET("?method=flickr.photos.geo.getLocation")
    suspend fun getPhotoLocationAsync(
        @Query("photo_id") photoId: String
    ): Response<PhotoLocationDataModel>
}