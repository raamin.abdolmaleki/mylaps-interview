package com.ramin.interview.feature.search.data

import com.ramin.interview.feature.search.MODULE_NAME
import com.ramin.interview.feature.search.data.repository.SearchRepositoryImpl
import com.ramin.interview.feature.search.data.retrofit.service.SearchRetrofitService
import com.ramin.interview.feature.search.domain.repository.SearchRepository
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit

internal val dataModule = Kodein.Module("${MODULE_NAME}DataModule") {
    bind<SearchRepository>() with singleton {
        SearchRepositoryImpl(instance())
    }

    bind() from singleton { instance<Retrofit>().create(SearchRetrofitService::class.java) }
}
