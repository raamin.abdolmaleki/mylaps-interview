package com.ramin.interview.feature.search.domain

import com.ramin.interview.feature.search.MODULE_NAME
import com.ramin.interview.feature.search.domain.usecase.SearchUseCase
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

internal val domainModule = Kodein.Module("${MODULE_NAME}DomainModule") {
    bind() from singleton { SearchUseCase(instance()) }
}
