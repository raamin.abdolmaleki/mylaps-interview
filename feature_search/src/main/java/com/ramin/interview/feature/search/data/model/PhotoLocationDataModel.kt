package com.ramin.interview.feature.search.data.model

import com.squareup.moshi.Json

class PhotoLocationDataModel (
    val photo: LocationPhoto,
    val stat: String
)


data class LocationPhoto (
    @field:Json(name = "id") val id: String,
    @field:Json(name = "location") val location: Location
)

data class Location (
    @field:Json(name = "latitude") val latitude: Double,
    @field:Json(name = "longitude") val longitude: Double
)
