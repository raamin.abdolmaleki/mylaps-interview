package com.ramin.interview.feature.search.presentation

import androidx.fragment.app.Fragment
import com.ramin.interview.feature.search.MODULE_NAME
import com.ramin.interview.feature.search.presentation.map.SearchViewModel
import com.ramin.interview.library.base.di.KotlinViewModelProvider
import org.kodein.di.Kodein
import org.kodein.di.android.x.AndroidLifecycleScope
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.scoped
import org.kodein.di.generic.singleton

internal val presentationModule = Kodein.Module("${MODULE_NAME}PresentationModule") {

    bind<SearchViewModel>() with scoped<Fragment>(AndroidLifecycleScope).singleton {
        KotlinViewModelProvider.of(context) {
            SearchViewModel(instance())
        }
    }
}
