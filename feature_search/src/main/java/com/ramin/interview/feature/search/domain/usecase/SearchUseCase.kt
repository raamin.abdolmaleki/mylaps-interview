package com.ramin.interview.feature.search.domain.usecase

import com.ramin.interview.feature.search.domain.model.SearchPhotoDomainModel
import com.ramin.interview.feature.search.domain.repository.SearchRepository
import com.ramin.interview.library.base.data.Resource
import com.ramin.interview.library.base.data.StatusCode
import kotlinx.coroutines.*

class SearchUseCase(
    private val searchRepository: SearchRepository
) {

    /**
     *  I found that this function does not have good performance,
     *  we can use coroutine channel, flow or other reactive tools for passing
     *  data immediately when it becomes available
     */
    suspend fun execute(
        lat: Double,
        lon: Double,
        query: String
    ): Resource<SearchPhotoDomainModel> = coroutineScope {

        val searchDataModel = withContext(Dispatchers.Default) {
            searchRepository.getPhotos(
                query = query,
                lat = lat,
                lon = lon
            )
        }
        if (searchDataModel.statusCode != StatusCode.OK) {
            Resource.error(searchDataModel.statusCode)
        } else {
            val deferredPhotoLocations = searchDataModel.response!!.photos.photo.map {
                async { searchRepository.getPhotoLocation(it.id) }
            }.awaitAll()

            val validPhotoLocations =
                deferredPhotoLocations.filter { it.statusCode == StatusCode.OK }
            Resource(
                response = SearchPhotoDomainModel(
                    photo = validPhotoLocations.map { it.response!! },
                    search = searchDataModel.response!!
                ),
                statusCode = StatusCode.OK
            )
        }

    }
}
