package com.ramin.interview.feature.search.data.model

import com.squareup.moshi.Json

data class SearchPhotoDataModel(
    val photos: Photos,
    val stat: String
)

data class Photos(
        @field:Json(name = "page") val page: Long,
        @field:Json(name = "pages") val pages: Long,
        @field:Json(name = "perpage") val perPage: Long,
        @field:Json(name = "total") val total: String,
        @field:Json(name = "photo") val photo: List<SearchPhoto>
)

data class SearchPhoto(
        @field:Json(name = "id") val id: String,
        @field:Json(name = "owner") val owner: String,
        @field:Json(name = "secret") val secret: String,
        @field:Json(name = "server") val server: String,
        @field:Json(name = "farm") val farm: Long,
        @field:Json(name = "title") val title: String,
        @field:Json(name = "ispublic") val isPublic: Long,
        @field:Json(name = "isfriend") val isFriend: Long,
        @field:Json(name = "isfamily") val isFamily: Long
)

fun SearchPhoto.getMarkerUrl(): String {
        return "https://live.staticflickr.com/${this.server}/${this.id}_${this.secret}_q.jpg"
}

fun SearchPhoto.getLargeUrl(): String {
        return "https://live.staticflickr.com/${this.server}/${this.id}_${this.secret}_b.jpg"
}