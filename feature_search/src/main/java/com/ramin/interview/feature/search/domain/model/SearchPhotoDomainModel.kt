package com.ramin.interview.feature.search.domain.model

import com.ramin.interview.feature.search.data.model.PhotoLocationDataModel
import com.ramin.interview.feature.search.data.model.SearchPhotoDataModel

class SearchPhotoDomainModel (
    val photo: List<PhotoLocationDataModel>,
    val search: SearchPhotoDataModel
)
