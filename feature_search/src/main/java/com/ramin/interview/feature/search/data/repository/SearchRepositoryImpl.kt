package com.ramin.interview.feature.search.data.repository

import com.ramin.interview.feature.search.data.model.PhotoLocationDataModel
import com.ramin.interview.feature.search.data.model.SearchPhotoDataModel
import com.ramin.interview.feature.search.data.retrofit.service.SearchRetrofitService
import com.ramin.interview.feature.search.domain.repository.SearchRepository
import com.ramin.interview.library.base.data.Resource
import com.ramin.interview.library.base.data.safeApiCall

internal class SearchRepositoryImpl(
    private val searchRetrofitService: SearchRetrofitService
) : SearchRepository {

    override suspend fun getPhotos(
        lat: Double,
        lon: Double,
        query: String
    ): Resource<SearchPhotoDataModel> {
        return safeApiCall {
            searchRetrofitService.getPhotosAsync(
                text = query,
                lat = lat,
                lon = lon
            )
        }
    }

    override suspend fun getPhotoLocation(photoId: String): Resource<PhotoLocationDataModel> {
        return safeApiCall {
            searchRetrofitService.getPhotoLocationAsync(photoId)
        }
    }
}
