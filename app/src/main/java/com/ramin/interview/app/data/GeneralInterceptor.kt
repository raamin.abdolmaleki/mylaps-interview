package com.ramin.interview.app.data

import com.ramin.interview.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class GeneralInterceptor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val url = chain.request().url.newBuilder()
            .addQueryParameter("api_key", BuildConfig.API_KEY)
            .addQueryParameter("nojsoncallback", "1")
            .addQueryParameter("format", "json")
            .build()

        val requestBuilder = chain.request().newBuilder().url(url)

        return chain.proceed(requestBuilder.build())
    }
}
