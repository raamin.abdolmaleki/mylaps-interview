package com.ramin.interview.app

import android.content.Context
import com.google.android.play.core.splitcompat.SplitCompatApplication
import com.ramin.interview.BuildConfig
import com.ramin.interview.app.feature.FeatureManager
import com.ramin.interview.app.kodein.FragmentArgsExternalSource
import com.ramin.interview.appModule
import com.ramin.interview.library.base.baseModule
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import timber.log.Timber

@Suppress("unused")
class InterviewTestApplication : SplitCompatApplication(), KodeinAware {
    override val kodein = Kodein.lazy {
        import(androidXModule(this@InterviewTestApplication))
        import(baseModule)
        import(appModule)
        importAll(FeatureManager.kodeinModules)

        externalSources.add(FragmentArgsExternalSource())
    }

    private lateinit var context: Context

    override fun onCreate() {
        super.onCreate()

        context = this

        initTimber()
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}
