package com.ramin.interview.library.base.data

enum class StatusCode(val code: Int){
    OK(200),
    BAD_REQUEST(400),
    UNAUTHORIZED(401),
    FORBIDDEN(403),
    NOT_FOUND(404),
    INTERNAL_SERVER_ERROR(500),
    UNKNOWN(1004);
}

fun getEnumByCode(statusCode: Int): StatusCode {
    return when (statusCode) {
        in 200..299 -> StatusCode.OK
        400 -> StatusCode.BAD_REQUEST
        401 -> StatusCode.UNAUTHORIZED
        403 -> StatusCode.FORBIDDEN
        404 -> StatusCode.NOT_FOUND
        in 500..599 -> StatusCode.INTERNAL_SERVER_ERROR
        else -> StatusCode.UNKNOWN
    }
}

