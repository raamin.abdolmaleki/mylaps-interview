package com.ramin.interview.library.base.data

import retrofit2.Response
import timber.log.Timber

inline fun <T> safeApiCall(call: () -> Response<T>): Resource<T> {
    return try {
        val res = call.invoke()
        Resource.result(res)
    } catch (e: Exception) {
        Timber.tag("SAFE_API_CALL").d(e.toString())
        Resource.unknownError()
    }
}
