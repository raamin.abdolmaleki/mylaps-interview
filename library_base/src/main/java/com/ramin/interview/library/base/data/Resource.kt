package com.ramin.interview.library.base.data

import retrofit2.Response

class Resource<T>(
        val response: T? = null,
        val statusCode: StatusCode
) {
    companion object {

        fun <T> unknownError(): Resource<T> {
            return Resource(statusCode = StatusCode.UNKNOWN)
        }

        @JvmStatic
        fun <T> error(errorCode: StatusCode): Resource<T> {
            return Resource(statusCode = errorCode)
        }

        fun <T> result(response: Response<T>): Resource<T> {
            return Resource(
                    statusCode = getEnumByCode(response.code()),
                    response = response.body()
            )
        }
    }
}
