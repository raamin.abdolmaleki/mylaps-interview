import AndroidConfig.DEFAULT_DIMENSION

object AndroidConfig {
    const val COMPILE_SDK_VERSION = 29
    const val MIN_SDK_VERSION = 21
    const val TARGET_SDK_VERSION = 29
    const val BUILD_TOOLS_VERSION = "29.0.3"

    const val VERSION_CODE = 1
    const val VERSION_NAME = "1.0"

    const val ID = "com.ramin.interview"
    const val TEST_INSTRUMENTATION_RUNNER = "android.support.test.runner.AndroidJUnitRunner"

    const val DEFAULT_DIMENSION = "build"
}

interface ProductFlavor {

    companion object {
        const val PRODUCTION = "production"
        const val API_BASE_URL = "API_BASE_URL"
        const val API_KEY = "API_KEY"
    }

    val applicationIdSuffix: String
    val apiBaseUrl: String
    val apiKey: String
    val dimension: String
}

interface BuildType {

    companion object {
        const val RELEASE = "release"
        const val DEBUG = "debug"
    }

    val isMinifyEnabled: Boolean
}

object BuildTypeDebug : BuildType {
    override val isMinifyEnabled = false
}

object BuildTypeRelease : BuildType {
    override val isMinifyEnabled = false
}

object ProductFlavorProduction : ProductFlavor {
    override val applicationIdSuffix = ""
    override val apiBaseUrl = "\"https://api.flickr.com/services/rest/\""
    override val apiKey = "\"c857332c24f8a97c22b9eee3e7b3553c\""
    override val dimension = DEFAULT_DIMENSION
}
