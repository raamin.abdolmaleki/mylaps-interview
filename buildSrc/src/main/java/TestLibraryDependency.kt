private object TestLibraryVersion {
    const val JUNIT = "4.13.2"
    const val TEST_RUNNER = "1.0.2"
    const val ARCH = "2.1.0"
    const val MOCKITO = "1.10.19"
}

object TestLibraryDependency {
    const val JUNIT = "junit:junit:${TestLibraryVersion.JUNIT}"
    const val TEST_RUNNER = "com.android.support.test:runner:${TestLibraryVersion.TEST_RUNNER}"

    const val MOCKITO = "org.mockito:mockito-core:${TestLibraryVersion.MOCKITO}"
    const val COROUTINES_TEST = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${CoreVersion.COROUTINES_ANDROID}"
    const val ANDROID_X_TEST = "androidx.arch.core:core-testing:${TestLibraryVersion.ARCH}"
}
