object CoreVersion {
    const val KOTLIN = "1.4.30"
    const val COROUTINES_ANDROID = "1.4.2"

    const val NAVIGATION = "2.3.0-alpha06"
}
